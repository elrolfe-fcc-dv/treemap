let movieData;

const displayData = () => {
  const tree = d3.select("#treemap");
  const treeWidth = +tree.attr("width");
  const treeHeight = +tree.attr("height");

  const legend = d3.select("#legend");

  const treemap = d3.treemap()
    .tile(d3.treemapResquarify)
    .size([treeWidth, treeHeight])
    .paddingInner(1);

  const root = d3.hierarchy(movieData)
    .eachBefore(d => d.data.id = (d.parent ? d.parent.data.id + "." : "") + d.data.name)
    .sum(d => d.value)
    .sort((a, b) => b.value - a.value);

  const fader = function(color) { return d3.interpolateRgb(color, "#ffffff")(0.2); };
  const color = d3.scaleOrdinal(d3.schemeCategory10.map(fader));
  treemap(root);

  const legendItem = legend.selectAll("g")
    .data(root.children)
    .enter()
    .append("g")
      .attr("transform", (d, i) => `translate(${(i % 3) * 200}, ${Math.floor(i / 3) * 50})`)

  legendItem.append("rect")
    .attr("class", "legend-item")
    .attr("fill", d => color(d.data.id))
    .attr("width", 35)
    .attr("height", 35);

  legendItem.append("text")
    .attr("fill", "#ffffff")
    .attr("x", 50)
    .attr("y", 25)
    .text(d => d.data.name);

  const cell = tree.selectAll("g")
    .data(root.leaves())
    .enter()
    .append("g")
      .attr("transform", d => `translate(${d.x0}, ${d.y0})`);

  cell.append("rect")
    .attr("id", d => d.data.id)
    .attr("class", "tile")
    .attr("data-name", d => d.data.name)
    .attr("data-category", d => d.data.category)
    .attr("data-value", d => d.data.value)
    .attr("width", d => d.x1 - d.x0)
    .attr("height", d => d.y1 - d.y0)
    .attr("fill", d => color(d.parent.data.id))
    .on("mouseover", d => {
      d3.select("#name").text(d.data.name);
      d3.select("#category").text(d.data.category);
      d3.select("#value").text(d3.format(",d")(d.data.value));
      d3.select("#tooltip")
        .attr("data-value", d.data.value)
        .style("left", (d3.event.pageX + 15) + "px")
        .style("top", (d3.event.pageY + 15) + "px")
        .style("opacity", 1);
    })
    .on("mouseout", () => {
      d3.select("#tooltip")
        .style("opacity", 0);
    });

  cell.append("clipPath")
    .attr("id", d => `clip-${d.data.id}`)
    .append("use")
      .attr("xlink:href", d => `#${d.data.id}`);

  cell.append("text")
    .attr("clip-path", d => `url(#clip-${d.data.id})`)
    .selectAll("tspan")
      .data(d => d.data.name.split(/(?=[A-Z][^A-Z])/g))
      .enter()
      .append("tspan")
        .attr("x", 4)
        .attr("y", (d, i) => 13 + i * 10)
        .text(d => d);

  d3.select("#title").text("Movie Sales");
  d3.select("#description").text("Top 100 Highest Grossing Movies Grouped by Genre");
};

document.addEventListener("DOMContentLoaded", () => {
  d3.json("https://cdn.rawgit.com/freeCodeCamp/testable-projects-fcc/a80ce8f9/src/data/tree_map/movie-data.json")
  .then((data) => {
    movieData = data;
    displayData();
  })
  .catch(err => {
    d3.select("#title").text("Error");
    d3.select("#subtitle").text(err.message || err || "Unknown Error");
  });
});
